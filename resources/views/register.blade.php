<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Hari 1 - Berlatih HTML</title>
</head>
<body>
  <h1>Buat Account Baru!</h1>
  <h3>Sign Up form</h3>
  <form action="/proses" method="POST">@csrf
    <label for="first_name">First Name:</label><br>
    <br>
    <input type="text" name="nama_depan" id="first_name"><br>
    <br>
    <label for="last_name">Last Name:</label><br>
    <br>
    <input type="text" name="nama_blk" id="last_name"><br>
    <br>

    <label for="">Gender:</label><br>
    <br>
    <input type="radio" id="male"  value="male">
    <label for="male">Male</label><br>
    <input type="radio" id="female"  value="female">
    <label for="female">Female</label><br>
    <input type="radio" id="other"  value="other">
    <label for="other">Other</label><br>
    <br>

    <label >Nationality:</label><br>
    <br>
    <select>
      <option>Indonesia</option>
      <option>Malaysia</option>
      <option>Singapore</option>
      <option>Australia</option>
    </select><br>
    <br>
    <label for="">Language Spoken:</label><br>
    <br>
    <input type="checkbox" id="bahasa">
    <label for="bahasa">Bahasa Indonesia</label><br>
    <input type="checkbox" id="english" >
    <label for="english">English</label><br>
    <input type="checkbox" id="other">
    <label for="other">Other</label><br>
    <br>

    <label>Bio:</label><br>
    <br>
    <textarea rows="10" cols="30"></textarea>
    <br>
    <input type="submit" value="Sign Up!">
  </form>
</body>
</html>