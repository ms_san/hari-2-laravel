<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('register');
    }

    public function proses(Request $request){
        $nama_depan = $request->nama_depan;
        $nama_blk = $request->nama_blk;
        return view('/welcome', compact('nama_depan','nama_blk'));
}
}
